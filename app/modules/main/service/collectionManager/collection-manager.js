//This is a service which abstract all the collection data management for other singleton service who calls the functions
//Is a wrapper for buckets.Dictionary.
angular.module('main').factory('collectionManager', function($log) {

    var collectionManager = {
        /**
         * getCollection give back the array contained inside the collection
         * @param  {object} collection  bucket collection 
         * @return {array}              returns an array containing all the data object
         */
        getCollection: function getCollection(collection) {
            return collection.values();
        },
        /**
         * setElement sets a new o replace an element inside the collection
         * @param {object} collection bucket collection 
         * @param {number} id         key used for dictionary
         * @param {null} 
         */
        setElement: function(collection, id, object) {
            if (_.isNull(id) || _.isUndefined(id)) {
                $log.error("setElement ID has to be defined");
                return false;
            }
            if (_.isNull(object) || _.isUndefined(object) || _.isEmpty(object)) {
                $log.error("setElement object has to be defined");
                return false;
            }
            collection.set(id, object);
        },
        /**
         * getElement get the element identified with an Id from the collection
         * @param   {object} collection bucket collection 
         * @param   {number} id         key used for dictionary
         * @return  {object}           object containing the element, null if not found
         */
        getElement: function(collection, id){
        	if (_.isNull(id) || _.isUndefined(id)) {
                $log.error("setElement ID has to be defined");
                return false;
            }
        	return collection.get(id);
        },
        /**
         * getNewId is used for getting a new Id managed as a progressive id from a collection
         * @param {object}  collection bucket collection 
         * @return {int}    number for the next id progressive
         */
        getNewId: function(collection){
            return collection.size()+1;
        }
    };

    return collectionManager;
});
