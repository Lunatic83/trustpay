/**  vehicles service to manage all vehicles information, this service is an interface, please note that we can easely switch data persitences from bucket Dictionary
*  	to backend resources (RESTful API) using $resources or Restangular.
**/
angular.module('main').factory('vehicles', function(collectionManager) {
    var vehiclesCollection = new buckets.Dictionary();

    vehiclesCollection.set(1, { id: 1, status: "booked", make: "Alfa Romeo", colour: "Yellow", wheels: 4, peoples: 5, customer_rent: { id: 9, name: 'Nick Mason', email: 'nick.mason@me.co.uk' } });
    vehiclesCollection.set(2, { id: 2, status: "available", make: "Ford", colour: "Green", wheels: 4, peoples: 5, customer_rent: null });
    vehiclesCollection.set(3, { id: 3, status: "available", make: "Ferrari", colour: "Red", wheels: 4, peoples: 2, customer_rent: null });
    vehiclesCollection.set(4, { id: 4, status: "available", make: "Renault", colour: "Black", wheels: 4, peoples: 5, customer_rent: null });
    vehiclesCollection.set(5, { id: 5, status: "available", make: "Citroen", colour: "Green", wheels: 4, peoples: 5, customer_rent: null });
    vehiclesCollection.set(6, { id: 6, status: "available", make: "Maserati", colour: "Grey", wheels: 4, peoples: 4, customer_rent: null });

    var vehicles = {
    	/**
    	 * getFleetCollection return an objects array containing all Fleet Vehicles
    	 * @return {array} the entire fleet
    	 */
        getFleetCollection: function getFleetCollection() {
            return collectionManager.getCollection(vehiclesCollection);
        },
        /**
         * addVehicle to the fleet
         * @param {object} vehicle to add 
         */
        addVehicle: function addVehicle(vehicle) {
            var self = this;
            vehicle.id = collectionManager.getNewId(vehiclesCollection);
            collectionManager.setElement(vehiclesCollection, vehicle.id, vehicle);
        },
        /**
         * updateVehicle to fleet
         * @param {object} vehicle to update 
         * @return {null}   
         */
        updateVehicle: function updateVehicle(vehicle) {
            collectionManager.setElement(vehiclesCollection, vehicle.id, vehicle);
        }
    };

    return vehicles;
});
