/**  Customer service to manage all customer information, this service is an interface, please note that we can easely switch data persitences from bucket Dictionary
*  	to backend resources (RESTful API) using $resources or Restangular.
**/
angular.module('main').factory('customers', function(collectionManager) {
    var customersCollection = new buckets.Dictionary();

    customersCollection.set(1, { id: 1, name: 'Paul Reed', email: 'paul.reed@me.co.uk' });
    customersCollection.set(2, { id: 2, name: 'John Green', email: 'john.green@me.co.uk' });
    customersCollection.set(3, { id: 3, name: 'Michael Finch', email: 'michael.finch@me.co.uk' });
    customersCollection.set(4, { id: 4, name: 'Joss Gerrard', email: 'joss.gerrard@me.co.uk' });
    customersCollection.set(5, { id: 5, name: 'Steve Jackson', email: 'steve.jacksonaul.@me.co.uk' });
    customersCollection.set(6, { id: 6, name: 'David Gilmour', email: 'david.gilmour@me.co.uk' });
    customersCollection.set(7, { id: 7, name: 'Roger Waters', email: 'roger.waters@me.co.uk' });
    customersCollection.set(8, { id: 8, name: 'Richard Wright', email: 'richard.wright@me.co.uk' });
    customersCollection.set(9, { id: 9, name: 'Nick Mason', email: 'nick.mason@me.co.uk' });
    customersCollection.set(10, { id: 10, name: 'Mike Oldfield', email: 'mike.olfield@me.co.uk' });

    var customers = {
        /**
         * getCollection return an objects array containing all the customers stored
         * @return {array} the entire fleet
         */
        getCollection: function getCollection() {
            return collectionManager.getCollection(customersCollection);
        },
        /**
         * getCustomer return a single customer
         * @param  {[type]} id to idenfity the customer
         * @return {object}    customer
         */
        getCustomer: function getCustomer(id) {
            return collectionManager.getElement(customersCollection, id);
        }
    };

    return customers;
});
