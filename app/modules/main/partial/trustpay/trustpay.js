angular.module('main').controller('TrustPayCtrl', function($scope, $mdDialog, vehiclesData, customersData, vehicles, customers) {
    $scope.vehiclesData = vehiclesData;
    $scope.customersData = customersData;

    $scope.filterStatus = undefined;

    /**
     * function binding event to addVehicle button     
     * @param  {object} ev      event
     */
    $scope.addVehicle = function(ev) {
        $mdDialog.show({
                controller: AddVehicleController,
                templateUrl: 'modules/main/partial/dialog/add-vehicle.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    customersData: $scope.customersData,
                    vehicleEdit: null
                }
            })
            .then(function(result) {
                if (result.answer === 'save') {
                    if (result.vehicle.id) {
                        //update
                        vehicles.updateVehicle(result.vehicle);
                    } else {
                        //add
                        vehicles.addVehicle(result.vehicle);
                    }
                    $scope.vehiclesData = vehicles.getFleetCollection();

                }
            }, function() {});
    };

    /**
     * function binding event to addVehicle button     
     * @param  {object} ev      event
     * @param  {object} vehicle containing data to edit     
     */
    $scope.editVehicle = function(ev, vehicle) {
        $mdDialog.show({
                controller: AddVehicleController,
                templateUrl: 'modules/main/partial/dialog/add-vehicle.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                locals: {
                    customersData: $scope.customersData,
                    vehicleEdit: vehicle
                }
            })
            .then(function(result) {
                if (result.answer === 'save') {
                    if (result.vehicle.id) {
                        //update
                        vehicles.updateVehicle(result.vehicle);
                    } else {
                        //add
                        vehicles.addVehicle(result.vehicle);
                    }
                    $scope.vehiclesData = vehicles.getFleetCollection();

                }
            }, function() {});
    }

    /**
     * AddVehicleController is the Dialgo controller with all the function binded to the view     
     */
    function AddVehicleController($scope, $log, $mdDialog, customersData, vehicleEdit, customers) {
        if (vehicleEdit) {
            $scope.vehicle = vehicleEdit;
            if ($scope.vehicle.customer_rent) {
                $scope.customer_id = $scope.vehicle.customer_rent.id;
            } else {
                $scope.customer_id = null;
            }
        } else {
            $scope.vehicle = {};
        }
        $scope.statusList = ['available', 'booked'];
        $scope.customersData = customersData;

        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function(answer) {
            var result = { answer: answer, vehicle: $scope.vehicle };
            $mdDialog.hide(result);
        };
        $scope.isFormInvalid = function() {
            return $scope.addVehicleForm && $scope.addVehicleForm.$invalid;
        };
        $scope.changeCustomer = function() {
            if (!$scope.customer_id) {
                $log.warning('Customer id is not defined');
                return false;
            }
            $scope.vehicle.status = 'booked';
            $scope.vehicle.customer_rent = customers.getCustomer(parseInt($scope.customer_id));
        };
        $scope.changeStatus = function() {
            if ($scope.vehicle.status === 'available') {
                $scope.vehicle.customer_rent = undefined;
                $scope.customer_id = undefined;
            }
        }
    }
});
