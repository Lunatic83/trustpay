angular.module('main', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate']);

angular.module('main').config(function($stateProvider) {

    $stateProvider.state('trustpay', {
        url: '/trustpay',
        templateUrl: 'modules/main/partial/trustpay/trustpay.html',
        controller: 'TrustPayCtrl',
        resolve: {
            vehiclesData: function(vehicles) {
                return vehicles.getFleetCollection();
            },
            customersData: function(customers) {
                return customers.getCollection();
            }
        }
    });
    /* Add New States Above */


});
