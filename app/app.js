angular.module('trustpayGaspareScherma', ['ui.bootstrap', 'ui.utils', 'ui.router', 'ngAnimate', 'templates', 'main','ngMaterial','ngMessages']);

angular.module('trustpayGaspareScherma').config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });

    /* Add New States Above */
    $urlRouterProvider.otherwise('/trustpay');

});

angular.module('trustpayGaspareScherma').run(function($rootScope) {
    
    $rootScope.safeApply = function(fn) {
        var phase = $rootScope.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

});
